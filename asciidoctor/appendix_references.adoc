[appendix]
== References

[bibliography]
- [[[AIDA]]] _AIDA: Abstract Interfaces for Data Analysis_ ( http://aida.freehep.org/ )
- [[[CLHEP_UNITS]]] _CLHEP Units_ ( https://proj-clhep.web.cern.ch/proj-clhep/manual/UserGuide/Units/units.html )
- [[[CMT]]] _CMT configuration management environment_ ( http://www.cmtsite.net/ )
- [[[GAUDI_ADD]]] _GAUDI - Architecture Design Report_ ( http://cds.cern.ch/record/691746[LHCb 98-064 COMP] )
- [[[GAUDI_DEV_SUBSCRIBE]]] _Subscription to gaudi-developers_ ( https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=gaudi-developers )
- [[[GAUDI_DOXYGEN]]] _GAUDI online code documentation_ ( http://proj-gaudi.web.cern.ch/proj-gaudi/releases/latest/doxygen/ )
- [[[GAUDI_GEANT4]]] I. Belyaev, _Integration of GEANT4 with Gaudi_ ( https://cds.cern.ch/record/1745073/files/LHCb-TALK-2001-022.pdf[LHCb-TALK-2001-022] )
- [[[GAUDI_GIGA]]] I. Belyaev, _GiGa: Geant4 Interface for Gaudi Applications_ ( http://lhcb-comp.web.cern.ch/lhcb-comp/Frameworks/Gaudi/Documents/GiGa.pdf )
- [[[GAUDI_JIRA]]] _Gaudi JIRA Project Page_ ( https://its.cern.ch/jira/browse/GAUDI/?selectedTab=com.atlassian.jira.jira-projects-plugin:summary-panel )
- [[[GAUDI_LONGPAPER]]] G. Barrand et al., _GAUDI - A Software Architecture and Framework for building HEP Data Processing Applications_ ( http://lhcb-comp.web.cern.ch/lhcb-comp/General/Publications/longpap-a152.pdf[Proc CHEP 2000, Computer Physics Communications 140 (2001) 45-55] )
- [[[GAUDI_RELEASE_NOTES]]] _Release notes for Gaudi v{gaudi-version}_ ( http://proj-gaudi.web.cern.ch/proj-gaudi/releases/v{gaudi-version}/release.notes.php )
- [[[GAUDI_USER_REQ]]] _GAUDI - User Requirements Document_ ( http://cds.cern.ch/record/684466[LHCb 98-065 COMP] )
- [[[GAUDI_WEB]]] _Gaudi website_ ( http://proj-gaudi.web.cern.ch/proj-gaudi/ )
- [[[LHCB_CODING]]] _Revised LHCb coding conventions_ ( http://cds.cern.ch/record/684691[LHCb 2001-054 COMP] )
- [[[LHCB_DETECTOR]]] G. Barrand et al., _The LHCb Detector Description Framework_ ( http://lhcb-comp.web.cern.ch/lhcb-comp/General/Publications/pap-a155.pdf[Proc CHEP 2000] )
- [[[LHCB_EVENT_DATA]]] _The new LHCb Event Data Model_ ( https://cds.cern.ch/record/681302[LHCb 2001-142 COMP] )
- [[[LHCB_GAUDI]]] _LHCb-specific Gaudi documentation_ ( http://cern.ch/lhcb-comp/Frameworks/Gaudi/ )
- [[[LHCB_PERSISTENCY]]] G. Barrand et al., _Data Persistency Solution for LHCb_ ( http://lhcb-comp.web.cern.ch/lhcb-comp/General/Publications/pap-c153.pdf[Proc CHEP 2000] )
- [[[LHCB_TUTORIALS]]] _LHCb Software Training_ ( https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbSoftwareTutorials )
- [[[LHCB_UNITS]]] _LHCb Physical Units Convention_ ( http://lhcb-comp.web.cern.ch/lhcb-comp/Support/Conventions/units.pdf )


.. _chapter_introduction:

************
Introduction
************

Purpose of the document
=======================

This document is intended as a combination of user guide and tutorial for the use of the Gaudi application framework software. It is complemented principally by two other “documents”: the Architecture Design Document (ADD) [GAUDI_ADD]_, and the online auto-generated reference documentation [GAUDI_DOXYGEN]_. A third document [GAUDI_USER_REQ]_ lists the User Requirements and Scenarios that were used as input and validation of the architecture design.

All these documents and other information about Gaudi, including source code documentation, are available via the Gaudi home page [GAUDI_WEB]_. Documentation of the LHCb extensions to Gaudi, and a copy of this user guide, are available on the LHCb web at [LHCB_GAUDI]_. It is recommended that you also study the material of the LHCb Gaudi tutorials given at CERN, which is available at [LHCB_TUTORIALS]_. Should you wish to enrol on a future tutorial, you should send mail to `Markus Frank <mailto:Markus.Frank@cern.ch>`_.

The ADD contains a discussion of the architecture of the framework, the major design choices taken in arriving at this architecture and some of the reasons for these choices. It should be of interest to anyone who wishes to write anything other than private analysis code.

As discussed in the ADD the application framework should be usable for implementing the full range of offline computing tasks: the generation of events, simulation of the detector, event reconstruction, testbeam data analysis, detector alignment, visualisation, etc.

In this document we present the main components of the framework which are available in the current release of the software. It is intended to increment the functionality of the software at each release, so this document will also develop as the framework increases in functionality. Having said that, physicist users and developers actually see only a small fraction of the framework code in the form of a number of key interfaces. These interfaces should change very little if at all and the user of the framework cares very little about what goes on in the background.

Outline
=======

The document is organized as follows:

:ref:`chapter_framework_arch` is a short summary of the framework architecture, presented from an “Algorithm-centric” point of view, and re-iterating only a part of what is presented in the ADD.

:ref:`chapter_release_notes_and_install` contains a summary of the functionality which is present in the current release, and details of how to obtain and install the software.

:ref:`chapter_getting_started` discusses in some detail an example which comes with the framework library. It covers the main program, some of the basic aspects of implementing algorithms, the specification of job options and takes a look at how the code is actually executed. The subject of coding algorithms is treated in more depth in :ref:`chapter_writing_algorithms`.

:ref:`chapter_accessing_data` discusses the use of the framework data stores and event data. :ref:`chapter_modelling_event_data` gives pointers to guidelines for defining the LHCb event data model. :ref:`chapter_detector_description`, :ref:`chapter_histogram_facilities` and :ref:`chapter_n_tuple_and_event_collection` discuss the other types of data accessible via these stores: detector description data (material and geometry), histogram data and n-tuples.

:ref:`chapter_framework_services` deals with services available in the framework: job options, messages, particle properties, auditors, chrono & stat, random numbers, incidents, introspection. It also has a section on developing new services. Framework tools are discussed in :ref:`chapter_tools_and_toolsvc`, the use and implementation of converter classes in :ref:`chapter_converters`.

:ref:`chapter_scripting_and_interactivity` discusses scripting as a means of configuring and controlling the application interactively. This is followed by a description in :ref:`chapter_visualization_facilities` of how visualisation facilities might be implemented inside Gaudi.

:ref:`chapter_framework_pkgs_ifaces_libs` describes the package structure of Gaudi and discusses the different types of libraries in the distribution.

:ref:`chapter_analysis_utilities` gives pointers to the documentation for class libraries whose use we recommend within Gaudi.

The use of certain SICB facilities within Gaudi and the wrapping of FORTRAN code are discussed in :ref:`chapter_accessing_sicb_facilities`.

:ref:`appendix_references` contains a list of references. :ref:`appendix_component_options` lists the options which may be specified for the standard components available in the current release. :ref:`appendix_job_options` gives the details of the syntax and possible error messages of the job options compiler. Finally, :ref:`appendix_design_considerations` is a small guide to designing classes that are to be used in conjunction with the application framework.

Conventions
===========

WIP

Reporting problems
==================

WIP

Editor's note
=============

WIP

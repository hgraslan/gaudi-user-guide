.. _appendix_references:

**********************
Appendix A: References
**********************

.. [AIDA] AIDA: Abstract Interfaces for Data Analysis ( http://aida.freehep.org/ )
.. [CMT] CMT configuration management environment ( http://www.cmtsite.net/ )
.. [GAUDI_ADD] GAUDI - Architecture Design Report ( `LHCb 98-064 COMP <http://cds.cern.ch/record/691746>`_ )
.. [GAUDI_DOXYGEN] GAUDI online code documentation ( http://proj-gaudi.web.cern.ch/proj-gaudi/releases/latest/doxygen/ )
.. [GAUDI_GEANT4] I. Belyaev, *Integration of GEANT4 with Gaudi* ( `LHCb-TALK-2001-022 <https://cds.cern.ch/record/1745073/files/LHCb-TALK-2001-022.pdf>`_ )
.. [GAUDI_GIGA] I. Belyaev, *GiGa: Geant4 Interface for Gaudi Applications* ( http://lhcb-comp.web.cern.ch/lhcb-comp/Frameworks/Gaudi/Documents/GiGa.pdf )
.. [GAUDI_LONGPAPER] G. Barrand et al., *GAUDI - A Software Architecture and Framework for building HEP Data Processing Applications* ( `Proc CHEP 2000, Computer Physics Communications 140 (2001) 45-55 <http://lhcb-comp.web.cern.ch/lhcb-comp/General/Publications/longpap-a152.pdf>`_ )
.. [GAUDI_USER_REQ] GAUDI - User Requirements Document ( `LHCb 98-065 COMP <http://cds.cern.ch/record/684466>`_ )
.. [GAUDI_WEB] Gaudi website ( http://proj-gaudi.web.cern.ch/proj-gaudi/ )
.. [LHCB_CODING] Revised LHCb coding conventions ( `LHCb 2001-054 COMP <http://cds.cern.ch/record/684691>`_ )
.. [LHCB_DETECTOR] G. Barrand et al., *The LHCb Detector Description Framework* ( `Proc CHEP 2000 <http://lhcb-comp.web.cern.ch/lhcb-comp/General/Publications/pap-a155.pdf>`_ )
.. [LHCB_EVENT] The new LHCb Event Data Model ( `LHCb 2001-142 COMP <https://cds.cern.ch/record/681302>`_ )
.. [LHCB_GAUDI] LHCb-specific Gaudi documentation ( http://cern.ch/lhcb-comp/Frameworks/Gaudi/ )
.. [LHCB_PERSISTENCY] G. Barrand et al., *Data Persistency Solution for LHCb* ( `Proc CHEP 2000 <http://lhcb-comp.web.cern.ch/lhcb-comp/General/Publications/pap-c153.pdf>`_ )
.. [LHCB_TUTORIALS] LHCb Software Training ( https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbSoftwareTutorials )
.. [LHCB_UNITS] LHCb Physical Units Convention ( http://lhcb-comp.web.cern.ch/lhcb-comp/Support/Conventions/units.pdf )

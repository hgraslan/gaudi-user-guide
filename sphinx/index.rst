.. Gaudi documentation master file, created by
   sphinx-quickstart on Thu Feb 18 11:18:51 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##################
Gaudi User's Guide
##################

.. toctree::
   :maxdepth: 2
   
   chapter_introduction
   chapter_framework_arch
   chapter_release_notes_and_install
   chapter_getting_started
   chapter_writing_algorithms
   chapter_accessing_data
   chapter_modelling_event_data
   chapter_detector_description
   chapter_histogram_facilities
   chapter_n_tuple_and_event_collection
   chapter_framework_services
   chapter_tools_and_toolsvc
   chapter_converters
   chapter_scripting_and_interactivity
   chapter_visualization_facilities
   chapter_framework_pkgs_ifaces_libs
   chapter_analysis_utilities
   chapter_accessing_sicb_facilities
   
   appendix_references
   appendix_component_options
   appendix_job_options
   appendix_design_considerations
